#-*- coding: utf-8 -*-
import sqlite3

class Connection(object):

    def __init__(self):
        try:
            self.connection = sqlite3.connect('db/user_bd.db')
            self.cursor = self.connection.cursor()
            print("DB: Database Conected")
        except sqlite3.OperationalError as error:
            print("Error:", error)

    def createTables(self):
        try:
            self.cursor.execute("CREATE TABLE IF NOT EXISTS users(id_user text PRIMARY KEY, name text, lastname text, email text, address text, born_date text)")
            print("DB: Users Table created")
            self.cursor.execute("CREATE TABLE IF NOT EXISTS telephones(id_user text, telephone text)")
            print("DB: Telephones Table created")
        except sqlite3.OperationalError as error:
            print("Error:", error)

    def getConnection(self):
        return self.connection

    def getCursor(self):
        return self.cursor

    def closeConnection():
        self.connection.close()
        print("DB: Data Base Conected")

    def getUsers(self):
        return self.cursor.execute('SELECT * FROM users')

    def getTelephones(self):
        return self.cursor.execute('SELECT * FROM telephones')
