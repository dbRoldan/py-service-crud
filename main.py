#-*- coding: utf-8 -*-
from db.connection import Connection
from user_dto import User
from user_dao import UserDAO

if __name__ == '__main__':
    conn = Connection()
    conn.createTables()
    userDAO = UserDAO(conn)

    #user_aux = User('12345', 'Samuel', 'Langhorne', 'twain@email.com', 'Cometa Halley', '1835-11-30', ['1234567'])
    #userDAO.createUser(user_aux)

    #user = userDAO.getUser('256877')
    #user.setName("Robert")
    #userDAO.updateUser(user)

    #userDAO.deleteUser('12345')
