# Python Project
### BASIC CRUD
#### Application Deployment
To install the project's own dependencies you can use:
```sh
  → pipenv lock
```
To edit the project you can use the comment inside the project folder:
```sh
  → pipenv shell
```
#### Description
The relational model is based on which e will perform the crud of users is:

![RelationalModel](docs/relational_model.png "Relational Model")

#### Licencia:
MIT License
