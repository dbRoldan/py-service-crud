#-*- coding: utf-8 -*-
class User(object):
    def __init__(self, id, name, lastname, born_date, email, address, telephone):
        self._id = id
        self._name = name
        self._lastname = lastname
        self._born_date = born_date
        self._email = email
        self._address = address
        self._telephone = telephone

    def getId(self):
        return self._id

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getLastname(self):
        return self._lastname

    def setLastname(self, lastname):
        self._lastname = lastname

    def getBorn_date(self):
        return self._born_date

    def setBorn_date(self, bdate):
        self._born_date = bdate

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getAddress(self):
        return self._address

    def setAddress(self, address):
        self._address = address

    def getTelephone(self):
        return self._telephone
