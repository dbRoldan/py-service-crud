#-*- coding: utf-8 -*-
import sqlite3
from user_dto import User

class UserDAO(object):
    def __init__(self, connection):
        self.conn = connection.getConnection()
        self.cursor = connection.getCursor()

    def createUser(self, user):
        data_query = (user.getId(), user.getName(), user.getLastname(), user.getEmail(), user.getAddress(), user.getBorn_date())
        try:
            self.cursor.execute("INSERT INTO users VALUES (?,?,?,?,?,?)", data_query)
            self.conn.commit()
            print("TB: User Added")
        except sqlite3.OperationalError as error:
            print("TB Error:", error)

        for telephone in user.getTelephone():
            query = "INSERT INTO telephones VALUES (" + user.getId() + "," + telephone + ")"
            try:
                self.cursor.execute(query)
                self.conn.commit()
                print("TB: Telephone Added ")
            except sqlite3.OperationalError as error:
                print("TB Error:", error)

    def _getTelephones(self, id):
        telephones = []
        try:
            query = "SELECT * FROM telephones WHERE id_user =" + id
            self.cursor.execute(query)
            tels = self.cursor.fetchall()
            for telephone in tels:
                telephones.append(telephone[1])
        except sqlite3.OperationalError as error:
            print("TB Error:", error)
        return telephones

    def getUser(self, id):
        try:
            query = "SELECT * FROM users WHERE id_user =" + id
            self.cursor.execute(query)
            user_info = self.cursor.fetchall()
            telephones = self._getTelephones(id)
            user = User(user_info[0][0], user_info[0][1], user_info[0][2], user_info[0][4], user_info[0][5], user_info[0][3], telephones)
            return user
        except sqlite3.OperationalError as error:
            print("TB Error:", error)

    def updateUser(self, user):
        data_query = (user.getName(), user.getLastname(), user.getAddress(), user.getEmail(), user.getBorn_date(), user.getId())
        try:
            self.cursor.execute('UPDATE users SET name = ?, lastname = ?, email = ?, address = ?, born_date = ? where id_user = ?', data_query)
            self.conn.commit()
        except sqlite3.OperationalError as error:
            print("TB Error:", error)

    def deleteUser(self, id):
        query = "DELETE FROM users WHERE id_user ="+ id
        try:
            self.cursor.execute(query)
            self.conn.commit()
            print("TB: User deleted")
            try:
                query_tel  = "DELETE FROM telephones WHERE id_user ="+ id
                self.cursor.execute(query_tel)
                self.conn.commit()
                print("TB: Telephone deleted")
            except sqlite3.OperationalError as error:
                print("TB Error:", error)
        except sqlite3.OperationalError as error:
            print("TB Error:", error)
